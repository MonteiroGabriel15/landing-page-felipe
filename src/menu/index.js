import React from "react";
import './index.css'

const Menu = () => {
    return (
        <div className='menu-container'>
        <button className='home-button'> home </button>
        <button className='contact-button'> contato </button>
        <button className='about-button'> sobre mim </button>
        <button className='dark-mode'></button>
        </div>
    )
}
export default Menu