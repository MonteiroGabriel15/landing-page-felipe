import './App.css';
import Menu from './menu';
import Header from './header'
import Form from './form';

function App() {
  return (
    <div className='main'>
      <Header/>
      <Menu/>
      <Form/>
      <div className='showcase-container'>
        <p>Um pouco do meu trabalho
        imagem 1 imagem 2 imagem 3
        imagem 4 imagem 5 imagem 6
        </p>
      </div>

      <div className='about-container'>
        <p>Sobre mim</p>
      </div>
      <div className='footer-container'>
        <p>Insira o texto aqui</p>
      </div>
    </div>
  )
}

export default App;
